﻿using UnityEngine;
using System.Collections;

public class LabelDraw : MonoBehaviour {

	public void OnDrawGizmos() {
		UnityEditor.Handles.Label(transform.position, gameObject.name);
	}
}
