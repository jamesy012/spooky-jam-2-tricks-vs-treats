﻿using UnityEngine;
using System.Collections;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadScene(int a_SceneNumber) {
		UnityEngine.SceneManagement.SceneManager.LoadScene(a_SceneNumber);
	}

}
