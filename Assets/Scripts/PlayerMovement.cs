﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour {


	/// <summary>
	/// Bounds of this object,
	/// it will always try to be within these positions
	/// </summary>
	public Transform m_TopLeftBounds, m_BotRightBounds;

	/// <summary>
	/// speed of object times by delta time
	/// </summary>
	public float m_Speed = 20.0f;

	/// <summary>
	/// flag to check whether we should move to where the mouse was clicked or use input axis to move
	/// </summary>
	private bool m_KeyMovement = true;

	/// <summary>
	/// Position of click in world space
	/// </summary>
	private Vector3 m_ClickPos = Vector3.zero;

	/// <summary>
	/// if the total amount of horizontal and vertical movement is larger then this number then the movement will switch to key movement
	/// </summary>
	public float m_AxisAmountForSwap = 0.1f;

	private Rigidbody m_Rigidbody;

	// Use this for initialization
	void Start() {
		m_Rigidbody = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update() {
		float speed = m_Speed * Time.deltaTime;

		//get if there was a mouse click this frame
		if (Input.GetMouseButtonDown(0)) {
			//work where the click was in world space coords
			m_ClickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			m_ClickPos.z = transform.position.z;
			m_KeyMovement = false;

			m_ClickPos = OutOfBoundsCheck(m_ClickPos);
		}

		//work out keyboard movement for this fame
		Vector3 movementAmount = Vector3.zero;
		movementAmount.x = Input.GetAxis("Horizontal");
		movementAmount.y = Input.GetAxis("Vertical");


		//check if the user wanted to move the character this frame to leave mouse movement
		if (Mathf.Abs(movementAmount.x + movementAmount.y) > m_AxisAmountForSwap) {
			m_KeyMovement = true;
		}

		//move based on key inputs
		if (m_KeyMovement) {
			//transform.position += movementAmount * speed;
			//transform.position = OutOfBoundsCheck(transform.position);
			m_Rigidbody.MovePosition(OutOfBoundsCheck(transform.position + movementAmount * speed));
		}
		else {//move based on mouse click
			  //transform.position = Vector3.MoveTowards(transform.position, m_ClickPos, speed);
			m_Rigidbody.MovePosition(Vector3.MoveTowards(transform.position, m_ClickPos, speed));
		}
	}

	/// <summary>
	/// checks whether a point it past the set up bounds
	/// </summary>
	/// <param name="a_Pos">position to check</param>
	/// <returns>returns a corrected position</returns>
	private Vector3 OutOfBoundsCheck(Vector3 a_Pos) {

		if (a_Pos.x > m_BotRightBounds.position.x) {
			a_Pos.x = m_BotRightBounds.position.x;
		}
		else if (a_Pos.x < m_TopLeftBounds.position.x) {
			a_Pos.x = m_TopLeftBounds.position.x;
		}

		if (a_Pos.y < m_BotRightBounds.position.y) {
			a_Pos.y = m_BotRightBounds.position.y;
		}
		else if (a_Pos.y > m_TopLeftBounds.position.y) {
			a_Pos.y = m_TopLeftBounds.position.y;
		}

		return a_Pos;
	}
}
