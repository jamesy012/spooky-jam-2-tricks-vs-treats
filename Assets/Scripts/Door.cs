﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public int m_SceneNumber;

	public void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {
			UnityEngine.SceneManagement.SceneManager.LoadScene(m_SceneNumber);
		}
	}

}
